# Frequently Asked Questions

 - [Are you removing the software I use?](#are-you-removing-the-software-i-use)
 - [I can't find my software in the support level list!](#i-cant-find-my-software-in-the-support-level-list)
 - [Who is on this Software Policy Committee?](#who-is-on-this-software-policy-committee)
 - [Can I ask for my software to get a higher support level?](#can-i-ask-for-my-software-to-get-a-higher-support-level)

## Are you removing the software I use?

No. During the policy transition period all currently installed software will
be left as is while we do the work of populating lists, cleaning up build
process, writing test cases, and evaluating software.

When the transition is complete, some software currently installed at NERSC
will have been assigned a Minimal support level. Such software will remain in
place but its entry will be moved from default modulefiles to
extra_modulefiles.

## I can't find my software in the support level list!

The support level of your software is "Minimal" like all software not explictly
set otherwise. NERSC will not install "Minimal" software for you, but
we will help guide the use of supported build tools and package managers to
meet your needs.

During the software policy transition period, software present in modules
at the beginning of AY 2020 will be supported as if it were "Provided".

## Who is on this Software Policy Committee?

The NERSC Software Policy Committee is composed of NERSC staff members
representing the following several NERSC groups:

- User Engagement
- Operations Technology
- Infrastructure Services
- Data Science Engagement
- Data and Analytics Services
- Computational Systems
- Application Performance

## Can I ask for my software to get a higher support level?

Yes. User feedback and requests are an important factor used when the Software
Policy Committee evaluates software support levels.

However, keep in mind, that a large number of factors must also be considered
including: software popularity, usage, resources needed to support,
compatibility with our systems, suitability of alternatives, dependencies,
maturity of code base, strategic priorities, and many more.
