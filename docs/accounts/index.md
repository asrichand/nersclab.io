# Accounts

## Obtaining an account

In order to use the NERSC facilities you need:

 1. Access to an allocation of computational or storage resources as a member
of a project account.
 2. A user account with an associated user login name (also called username).

If you are not a member of a project that already has a NERSC allocation award, 
you may apply for an allocation. Please see Applying for your First Allocation. 
In particular, Exploratory Awards are available for new projects that wish to
investigate using NERSC resources, or who wish to port or develop new codes.

### How to get a New User account in an existing project/repository

 1. You can submit a request for a new NERSC account by using the
[NERSC New Account Request](https://iris.nersc.gov/add-user) form.
 2. Enter your preferred username, the project you are applying to join, contact
information, and organization. For best results, ask your project PI for the
name of the project to use (project names are often the letter "m" followed by
up to four digits, e.g., m9876).
 3. After you submit the request, you will be asked to agree to the NERSC 
Appropriate Use Policy. This is required in order for your account request to
proceed.
 4. Your account will undergo user vetting, in accordance with NERSC policies,
to verify your identity. Under some circumstances, there could be a delay
while this vetting takes place.
 5. The PI and PI Proxies for the project will be notified that your account
request has been submitted. They will review your account request in Iris and 
either approve or deny your request. If they approve your request, you will 
receive an email notification that you have been added to a project. If your 
request is denied, the PI/Proxy will enter a reason for denying the request 
and you will receive an email with this reason. You should then contact them 
to resolve the issue.
 6. Finally, you will receive an email with a link that will allow you to set 
your initial password. That link will expire if not used within 24 hours. If
the link has expired, you will need to send an email to accounts@nersc.gov to 
obtain a new link to set a password. 

!!! note
    If you are an existing NERSC user and need to be added to a new project,
    the project PI or one of their proxies can add you. You can also go to the 
    [NERSC New Account Request](https://iris.nersc.gov/add-user) page and enter
    the first two items, then select the "existing user" box.

## Managing Accounts

NERSC user accounts are managed in the [Iris](https://iris.nersc.gov) system.
For more information on how to use Iris, please see the 
[Iris documentation](../iris/iris-for-users.md).
