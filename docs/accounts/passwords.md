# Passwords

## Password and Account Protection

A user is given a username (also known as a login name) and associated
password that permits her/him to access NERSC resources.  This
username/password pair may be used by a single individual only:
*passwords must not be shared with any other person*. Users who
share their passwords will have their access to NERSC disabled.

Passwords must be changed as soon as possible after exposure or
suspected compromise.  Exposure of passwords and suspected compromises
must immediately be reported to NERSC at security@nersc.gov or the
Account Support Group, accounts@nersc.gov.

## Forgotten Passwords

If you forget your password or if it has recently expired, you can
reset your password by clicking the 'Reset your password' link on
the [Iris login page](https://iris.nersc.gov).  Enter your username
and, if MFA has been enabled, an MFA One-Time Password (OTP), and click OK. 
The Iris system will send you an email with a link to the password change page 
and a secret code. The secret code will be valid for 24 hours. In the 
[password resetting page](https://iris.nersc.gov/reset-password), enter your NERSC username, 
the secret code, and your new password. Your password should meet the password 
requirements explained below and must be 'safe' or 'very safe' according to the 
provided password strength meter.

If you still have a problem after trying that, you will need to
send an email to accounts@nersc.gov describing the steps you took and the 
problem you are having. The Account Support personnel will respond to you 
during normal NERSC business hours. 

## How To Change Your Password in Iris

All of NERSC's computational systems are managed by the LDAP protocol
and use the Iris password. Passwords cannot be changed directly on
the computational machines, but rather the Iris password itself
must be changed:

1.  Point your browser to [https://iris.nersc.gov](https://iris.nersc.gov) and log in.
2.  Click the Iris icon in the top left corner (this will take you to your account).
3.  Select the 'Details' tab.
4.  Click on the 'Reset password' box to the left of the 'Self-service User
    Info' section.
5.  In the dialog window that appears, enter the current password,
    and your new password. Your password should meet the password
    requirements explained in the next section below, and must be
    'safe' or 'very safe' according to the provided password strength
    meter.
6.  Click the 'Set Password' button at the bottom.

Passwords must be changed under any one of the following circumstances:

*  Immediately after someone else has obtained your password (do *NOT*
   give your password to anyone else).
*  As soon as possible, but at least within one business day after a
   password has been compromised or after you suspect that a password
   has been compromised.
*  On direction from NERSC staff.

Your new password must adhere to NERSC's password requirements.

## Password Requirements

NIST (National Institute of Standards and Technology) has updated
their Digital Identity Guidelines in [Special Publication
800-63B](https://pages.nist.gov/800-63-3/sp800-63b.html). Based on
the guidelines and LBNL's new password standard, NERSC has updated
password policy to bring it in closer alignment with the guidelines.
The vast majority of NERSC users utilize a second factor in addition
to their password when logging into systems and web apps, and this
further justifies a change in password policy.

When users select their own passwords for use at NERSC, the
following requirements must be satisfied.

*   The password must register as either 'safe' or 'very safe' on
    a password strength meter that is provided.
*   The enforced minimum length for accounts with MFA enabled is 8 characters, 
    but in practice it may be difficult to select one that registers as 'safe' 
    on the meter with such a short password. If MFA is not enabled for your
    account the minimum password length is 14 characters.
*   There is no character complexity rule regarding inclusion of
    uppercase/lowercase letters, digits and special characters.
*   Passwords must be changed every six months.
  
If you are struggling to come up with a good password, Iris can recommend one 
for you. Click on the 'Recommend a safe password' link beneath the 'New 
password' box in the 'Password reset' dialog window.

## Login Failures

Your login privileges will be disabled if you have ten login failures
while entering your password on a NERSC resource. You do not need
a new password in this situation. The login failures will be
automatically cleared after 5 minutes. No additional actions are
necessary.
